import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  userName: string;

  password: string; 

  constructor(private router:Router) { }

  ngOnInit() {
  }

  signInUser(){
    if(this.userName==='user' && this.password==='password'){
      console.log('your are logged in')
      this.router.navigate(['/home'],{state: {data: this.userName}})

    }
    console.log('eneter right credentials')
    
  }

  backClickHandler() {
    this.router.navigate(['../'])
  }

}
