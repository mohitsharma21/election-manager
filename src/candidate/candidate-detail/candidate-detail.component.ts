import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { CandidateService } from 'src/shared/candidate.service';

@Component({
  selector: "app-candidate-detail",
  templateUrl: "./candidate-detail.component.html",
  styleUrls: ["./candidate-detail.component.scss"]
})
export class CandidateDetailComponent implements OnInit {
  disabled: boolean;

  candidate: Candidate;

  availableRanges: any[];

  disableSubmit: boolean;

  userName:string;

  showToast:boolean;

  constructor(private router: Router, private service : CandidateService) {
    this.disabled = false;
    this.candidate = {} as Candidate;
    this.availableRanges = [
      { value: "", key: "Select" },
      { value: "1L", key: "1L" },
      { value: "10L", key: "10L" },
      { value: "1Cr", key: "1Cr" },
      { value: "10Cr", key: "10Cr" },
      { value: "100Cr", key: "100Cr" }
    ];
    this.disableSubmit = true;
    this.showToast = false
  }

  ngOnInit() {
    console.log(history.state.data);
    this.candidate = history.state.data || {};
    this.disabled = history.state.disabled;
    this.userName = history.state.userName;
  }

  createCandidateHandeler() {
    console.log(this.candidate);
    this.service.addCandidate(this.candidate).subscribe(
        data => {
          this.showToast = true;
          setTimeout(()=>{
            this.router.navigate(["../"],{state: {data:this.userName}});
          },2000)
        },
        err => {
          console.log({ err });
        }
      );
  }

  handleChangeField(value) {
    this.disableSubmit = true;
    if (
      this.candidate.name &&
      this.candidate.age &&
      this.candidate.bio &&
      this.candidate.criminalRecord &&
      this.candidate.income
    ) {
      this.disableSubmit = false;
    }
  }
  handleChange(event) {
    this.candidate.income = event.target.value;
    this.handleChangeField("income");
  }

  backClickHandler() {
    this.router.navigate(["../"],{state: {data:this.userName}});
  }
}
