import { Component, OnInit } from "@angular/core";
import { CandidateService } from "src/shared/candidate.service";
import { Router } from '@angular/router';

@Component({
  selector: "app-landing",
  templateUrl: "./landing.component.html",
  styleUrls: ["./landing.component.scss"]
})
export class LandingComponent implements OnInit {
  candidateModel: Candidate;

  availableRanges: any[];

  errorMessage: string;

  searchedCandidates : Candidate[];

  resetValue:boolean;

  userName:string;

  constructor(private service: CandidateService, private router: Router) {
    this.candidateModel = {} as Candidate;
    this.availableRanges = [
      { value: "", key: "Select" },
      { value: "1L", key: "1L" },
      { value: "10L", key: "10L" },
      { value: "1Cr", key: "1Cr" },
      { value: "10Cr", key: "10Cr" },
      { value: "100Cr", key: "100Cr" }
    ];
    this.errorMessage = "";
    this.searchedCandidates =[];
    this.resetValue = false;
  }

  ngOnInit() {
    this.userName = history.state.data || '';
  }

  handleChange(event) {
    this.candidateModel.income = event.target.value;
  }

  onApply() {
    this.errorMessage = "";
    let candidates: Candidate[];

    if (
      !this.candidateModel.name &&
      !this.candidateModel.age &&
      !this.candidateModel.constituency &&
      !this.candidateModel.income
    ) {
      this.errorMessage = "Enter some value to search for Candidate";
    } else {
      this.service.getCandidates().subscribe((i: any) => {
        candidates = i;
        console.log({ candidates });

        if (this.candidateModel.name) {
          candidates = candidates.filter(i =>
            i.name.toLowerCase().includes(this.candidateModel.name.toLowerCase())
          );
          console.log({ candidates });
        }
        if (this.candidateModel.age) {
          candidates = candidates.filter(i => i.age === this.candidateModel.age);
        }
        if (this.candidateModel.constituency) {
          candidates = candidates.filter(i =>
            i.constituency
              .toLowerCase()
              .includes(this.candidateModel.constituency.toLowerCase())
          );
        }
        if (this.candidateModel.income) {
          candidates = candidates.filter(
            i => i.income === this.candidateModel.income
          );
        }
        this.resetValue = false;
        this.searchedCandidates = candidates;
        console.log(this.searchedCandidates);
      });
    }
  }

  addCandidateHandler() {
    this.router.navigate(['/details'],{state: {data: {}, disabled : false, userName:this.userName}})
  }

  loginHandler(){
    this.router.navigate(['/login'])
  }
  logoutHandler() {
    this.userName='';
  }
  onReset() {
    this.candidateModel = {} as Candidate;
    this.resetValue = true;
    console.log('reset',this.candidateModel)
  }
}
