import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-candidate-list',
  templateUrl: './candidate-list.component.html',
  styleUrls: ['./candidate-list.component.scss']
})
export class CandidateListComponent implements OnInit {

  @Input() candidates;

  @Input() userName;
  
  constructor(private router : Router) { }

  ngOnInit() {
  }

  openCandidateDetails(candidate) {
    this.router.navigate(['/details'],{state: {data: candidate , disabled : true, userName:this.userName}})
  }

}
