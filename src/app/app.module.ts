import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from 'src/shared/shared.module';
import { HeaderComponent } from 'src/shared/header/header.component';
import { LandingComponent } from 'src/candidate/landing/landing.component';
import { CandidateModule } from 'src/candidate/candidate.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CandidateListComponent } from 'src/candidate/candidate-list/candidate-list.component';
import { CandidateDetailComponent } from 'src/candidate/candidate-detail/candidate-detail.component';
import { ToastComponent } from 'src/shared/toast/toast.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LandingComponent,
    CandidateListComponent,
    CandidateDetailComponent,
    ToastComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    CandidateModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
