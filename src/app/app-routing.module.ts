import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LandingComponent } from "src/candidate/landing/landing.component";
import { CandidateDetailComponent } from "src/candidate/candidate-detail/candidate-detail.component";
import { LoginComponent } from 'src/candidate/login/login.component';

const routes: Routes = [
  { path: "home", component: LandingComponent },
  { path: "", redirectTo: "home", pathMatch: "full" },
  { path: "details", component: CandidateDetailComponent },
  { path: "login", component: LoginComponent }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
