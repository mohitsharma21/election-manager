import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CandidateService {

  constructor(
    private http: HttpClient,
  ) { }

  getCandidates() {
    return this.http.get('http://localhost:3000/candidates')
  }

  addCandidate(candidate) {
    return this.http.post("http://localhost:3000/candidates", candidate)
  }
}
